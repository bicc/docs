# BICC docs

Additional documentation that does not ship directly with source code.

Contents:

- PlantUML C4 Diagram of the BICC platform


Requirements to develop PlantUML:

- Java
- [PlantUML](https://plantuml.com/starting) 